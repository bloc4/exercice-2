package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;


import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;

/**
 * Cette nouvelle interface permet d'implémenter 2 méthodes pour désérialiser des badges uniquement à partir des metadonnées ou de la position du badge dans le fichier Json
 */

public interface JSONWalletBonusDeserializer extends DirectAccessDatabaseDeserializer {
     DigitalBadge deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas) throws IOException ;
     DigitalBadge deserialize(WalletFrameMedia media, long pos) throws IOException;
 }
